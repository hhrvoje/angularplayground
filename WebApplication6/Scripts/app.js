﻿/// <reference path="jquery-2.1.0.js" />
/// <reference path="angular.js" />

(function() {
    var app = angular.module("app", []);

    app.controller("todoController", function($scope) {
        $scope.todos = [{ done: true, text: "bla" }];

        $scope.newText = "";
        $scope.newDone = false;

        $scope.remaining = function() {
            var n = 0;
            angular.forEach($scope.todos, function(todo) {
                if (todo.done) n += 1;
            });
            return n;
        };

        $scope.addTodo = function() {
            $scope.todos.push({ text: $scope.newText, done: $scope.newDone });
            $scope.newDone = false;
            $scope.newText = "";
        };
    });
})();




